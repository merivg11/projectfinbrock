const Router = require('express')
const router = new Router()
const componentRouter = require('./componentRouter')
const partnerRouter = require('./partnerRouter')

router.use('/partner', partnerRouter)
router.use('/component', componentRouter)

module.exports = router