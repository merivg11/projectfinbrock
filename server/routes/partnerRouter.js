const Router = require('express')
const router = new Router()
const partnerController = require('../controllers/partnerController')


router.post('/', partnerController.create)
router.get('/', partnerController.getAll)
router.get('/:id', partnerController.deleteOne)


module.exports = router
