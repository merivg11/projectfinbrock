const Router = require('express')
const router = new Router()
const componentController = require('../controllers/componentController')

router.post('/', componentController.create)
router.get('/', componentController.getAll)
router.get('/:id', componentController.getOne)
router.put('/:id', componentController.changeOne)


module.exports = router
