const sequelize = require('../db')
const {DataTypes} = require('sequelize')

// const User = sequelize.define('user', {
//     id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
//     email: {type: DataTypes.STRING, unique: true,},
//     password: {type: DataTypes.STRING},
//     role: {type: DataTypes.STRING, defaultValue: "USER"},
// })

const Component = sequelize.define('component', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    text: {type: DataTypes.STRING, unique: false, allowNull: false},
    group: {type: DataTypes.STRING, unique: false, allowNull: false},
    img: {type: DataTypes.STRING, allowNull: true},
    icon: {type: DataTypes.STRING, allowNull: true},
})

const Partner = sequelize.define('partner', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    img: {type: DataTypes.STRING, allowNull: false},
})

module.exports = {
    Component,
    Partner
}





