import React from "react";
import InfoCardList from "./components/InfoCardList"
import Header from "./components/Header"
import SectionFirst from "./components/SectionFirst"
import SectionSecond from "./components/SectionSecond"
import SectionSecondSmScreen from "./components/SectionSecondSmScreeen"
import './css/custom.css'
import './css/index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import SectionThird from "./components/SectionThird";
import SectionFourth from "./components/SectionFourth";
import Footer from "./components/Footer";

function App() {
  return (
      <div>
    < Header />
        <SectionFirst />
        <InfoCardList />
          <SectionSecond />
          <SectionSecondSmScreen />
          <SectionThird />
          < SectionFourth />
          <Footer />
      </div>
  );
}

export default App;
