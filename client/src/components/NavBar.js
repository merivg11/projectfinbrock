import React from 'react';
import '../css/index.css'
import '../css/custom.css'
import logo_ukr from '../images/logo/logo_ukr.png'
import facebook from '../images/icon/facebook.png'
import 'bootstrap/dist/css/bootstrap.min.css';


const NavBar = () => {
    console.log('111111111')
    return (
        <nav className="navbar bg-white navbar-expand-md navbar-light">
            <div className="container header-nav">
                <div className="header-nav__logo">
                    <a href="#">
                        <img src={logo_ukr} />
                    </a>
                </div>
                <div className="header-nav__tel">
                    <a href="tel:+380503830829">+38 (050) 383-08-29</a>
                </div>
                <button className="navbar-toggler border-0" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    <span className="navbar-toggler-icon-close"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end navbar-menu" id="navbarNavDropdown">
                    <div className="w-100 navbar-line"></div>
                    <div className="navbar-collapse-body w-100">
                        <div className="header-control__lang">
                            <a href="#">УКР</a>
                            <a href="#" className="active">РУ</a>
                        </div>
                        <ul className="navbar-nav w-100 justify-content-between flex-wrap">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="#">Главная</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">О брокере</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link" href="#" id="navbarDropdownMenuProduct" role="button"
                                   data-bs-toggle="dropdown"
                                   aria-expanded="false">
                                    Продукты
                                </a>
                                <ul className="dropdown-menu border-0" aria-labelledby="navbarDropdownMenuProduct">
                                    <li><a className="dropdown-item" href="#">Потребительский кредит</a></li>
                                    <li><a className="dropdown-item" href="#">Денежный кредит</a></li>
                                    <li><a className="dropdown-item" href="#">Страховые продукты</a></li>
                                    <li><a className="dropdown-item" href="#">Карточные продукты</a></li>
                                </ul>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link" href="#" id="navbarDropdownMenuLead" role="button"
                                   data-bs-toggle="dropdown"
                                   aria-expanded="false">
                                    Преимущества
                                </a>
                                <ul className="dropdown-menu border-0" aria-labelledby="navbarDropdownMenuLead">
                                    <li><a className="dropdown-item" href="#">Для банков</a></li>
                                    <li><a className="dropdown-item" href="#">Для ритейла</a></li>
                                </ul>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link" href="#" id="navbarDropdownMenuPartner" role="button"
                                   data-bs-toggle="dropdown"
                                   aria-expanded="false">
                                    Партнеры
                                </a>
                                <ul className="dropdown-menu border-0" aria-labelledby="navbarDropdownMenuPartner">
                                    <li><a className="dropdown-item" href="#">Финансовые</a></li>
                                    <li><a className="dropdown-item" href="#">Торговые</a></li>
                                    <li><a className="dropdown-item" href="#">Партнеры о нас</a></li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Новости</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Контакты</a>
                            </li>
                        </ul>
                        <div className="social-media">
                            <a href="mailto:broker-sup@sfr.kiev.ua">broker-sup@sfr.kiev.ua</a>
                            <a href="#">
                                <img src={facebook} />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
};

export default NavBar;