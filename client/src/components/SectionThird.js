import React from 'react';
import SectionThirdList from "./SectionThirdList";
import image from "../images/2.png"

const SectionThird = () => {
    return (
        <div className="container">
            <div className="section-third">
                <div className="section-third-info">
                    <div className="section-third-info__header">
                        За 5 лет работы “финансовому брокеру” удалось:
                    </div>
                    <div className="section-third-info__img">
                        <img src={image} />
                    </div>
                </div>
                < SectionThirdList />
            </div>
        </div>
    );
};

export default SectionThird;