import React from 'react';
import SectionThirdListItem from './SectionThirdListItem'

const SectionThirdList = () => {
    return (
        <div className="section-third-list">
            <SectionThirdListItem />
            <SectionThirdListItem />
            <SectionThirdListItem />
            <SectionThirdListItem />
            <SectionThirdListItem />
        </div>
    );
};

export default SectionThirdList;