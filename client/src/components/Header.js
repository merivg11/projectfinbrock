import React from 'react';
import NavBar from './NavBar'
import '../css/index.css'
import '../css/custom.css'
import 'bootstrap/dist/css/bootstrap.min.css';

const Header = () => {
    return (
        <header>
            <div className="header-control-wrapper">
                <div className="container header-control">
                    <div className="header-control__contact">
                        <a href="tel:+380503830829">+38 (050) 383-08-29</a>
                        <a href="mailto:broker-sup@sfr.kiev.ua">Broker-sup@sfr.kiev.ua</a>
                    </div>
                    <div className="header-control__lang">
                        <a href="#">УКР</a>
                        <a href="#" className="active">РУ</a>
                    </div>
                </div>
            </div>
            <NavBar />
        </header>
    );
};

export default Header;