import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import '../css/custom.css'

const SectionSecondSmScreen = () => {
    return (
        <div className="section-second-sm-screen">
            <div className="container">
                <div className="section-second-message">
                    <ul>
                        <li>
                            «Финансовый Брокер» (от слова «брокер» – посредник, который помогает осуществлению сделок
                        </li>
                        <li>
                            между заинтересованными сторонами) – бизнес-проект по эффективной организации
                        </li>
                        <li>
                            процесса потребительского экспресс-кредитования непосредственно в точках продаж.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default SectionSecondSmScreen;