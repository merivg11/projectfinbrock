import React from 'react';
import item from '../images/partner/1.png'

const SectionFourthItem = () => {
    return (
        <div className="section-fourth-slider_item">
            <img src={item} />
        </div>
    );
};

export default SectionFourthItem;